import os, sys, getopt
import pandas as pd

from configurator import Configurator
from os.path import isfile, join


class Concatenator:
    def __init__(self, project_name=None, sample_num=None, step_num=None, node_number=None):

        self.d4j_env = os.environ["D4J_HOME"]
        self.pwd = os.getcwd()
        # self.d4j_env = "/mnt/d/workdir/defects4j"
        # self.pwd = "/mnt/d/180603/Time"
        # self.pwd = "/mnt/c/Data/Chart"

        self.project_name = project_name
        self.data_category = "/method_data/"
        self.sample_number = sample_num
        self.step_number = step_num
        self.stmt_path = "/framework/bin/fluccs/method_stmt/"
        self.node_number = node_number
        self.train_data_origin_path = "train_data/origin_data/"
        self.train_data_ccm_path = "train_data/ccm_data/"
        self.train_data_ccm_x_path = "train_data/ccm_x_data/"
        self.train_result_data_origin_path = "train_result_data/origin_data/"
        self.train_result_data_ccm_path = "train_result_data/ccm_data/"
        self.total_result_data_origin_path = "total_data/rank_data/origin_data/"
        self.total_result_data_ccm_path = "total_data/rank_data/ccm_data/"
        self.base_path = self.pwd + "/outputs/" + self.project_name + "/"
        self.line_path = "method_data/line_data/"

    def generate_rank_data(self):

        if not os.path.exists(
                self.base_path + self.total_result_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number):
            os.makedirs(
                self.base_path + self.total_result_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number)

        # fault_info_path = self.base_path + "method_data/fault_data/origin_csv"

        file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path + self.line_path)]

        for file_number in file_numbers:
            file_number = str(file_number)

            line_data_path = self.base_path + self.line_path + "stmt_" + self.project_name + "_" + file_number + "_data.csv"
            suspicion_data_path = self.base_path + self.train_result_data_origin_path+"train_result/" + self.project_name + "_" + file_number + "/" + self.project_name + "_" + file_number + "_b_train_result_sample-" + str(
                self.sample_number) + "_step-" + self.step_number + "_nodes-" + self.node_number + ".csv"

            faulty_info_path = self.base_path + "method_data/fault_data/origin_csv/method_fault_" + file_number + ".csv"

            try:
                line_data = pd.read_csv(line_data_path, header=None)
                suspicion_data = pd.read_csv(suspicion_data_path, header=None)
                faulty_data = pd.read_csv(faulty_info_path, header=None)

            except (FileNotFoundError, pd.errors.EmptyDataError) as e:
                print(file_number)
                print(e)
                continue
            print(faulty_data)
            faulty_data.columns = ["class_name", "faulty_flag"]
            print(faulty_data)
            merged_data = pd.merge(line_data, suspicion_data, on=0)
            line_suspicion_data = merged_data.drop(columns=[2])
            header = ["rank", "class_name", "suspiciousness"]
            line_suspicion_data.columns = header

            merged_faulty_suspiciouness_data = pd.merge(line_suspicion_data, faulty_data, how="left",
                                                        on=["class_name"])

            merged_faulty_suspiciouness_data["faulty_flag"].fillna(0, inplace=True)
            total_result = merged_faulty_suspiciouness_data.sort_values(["suspiciousness"], ascending=False)
            total_data = total_result.reset_index(drop=True)
            total_data["rank"] = total_data.index
            total_data["percentile_rank"] = total_data.index / len(total_data)

            total_result_csv = pd.DataFrame(total_data.loc[total_data["faulty_flag"] == 1])
            total_result_csv.to_csv(
                self.base_path + self.total_result_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv",
                index=False)
            print(
                "Create : ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")

    def generate_rank_data_v1(self):
        # print(self.sample_number, self.step_number, self.node_number)

        if not os.path.exists(
                self.base_path + self.total_result_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number):
            os.makedirs(
                self.base_path + self.total_result_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number)

        # fault_info_path = self.base_path + "method_data/fault_data/origin_csv"

        file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path + self.line_path)]

        for file_number in file_numbers:
            file_number = str(file_number)

            line_data_path = self.base_path + self.line_path + "stmt_" + self.project_name + "_" + file_number + "_data.csv"
            suspicion_data_path = self.base_path + self.train_result_data_origin_path + "train_result/" + self.project_name + "_" + file_number + "/" + self.project_name + "_" + file_number + "_b_train_result_sample-" + str(
                self.sample_number) + "_step-" + self.step_number + "_nodes-" + self.node_number + ".csv"

            # faulty_info_path = self.base_path + "method_data/fault_data/origin_csv/method_fault_" + file_number + ".csv"
            fault_info_path = self.base_path + "method_data/fault_data/existing_csv/method_fault_" + self.project_name + "_" + file_number + "_.csv"

            try:
                # line_data = pd.read_csv(line_data_path, header=None)
                suspicion_data = pd.read_csv(suspicion_data_path, header=None)
                fault_data = pd.read_csv(fault_info_path, header=None)

            except (FileNotFoundError, pd.errors.EmptyDataError) as e:
                print(file_number)
                print(e)
                continue
            merged_data = pd.merge(fault_data, suspicion_data, how="left", on=0)

            merged_data = merged_data.sort_values(["1_y", 2], ascending=[False, True])
            merged_data = merged_data.reset_index(drop=True)
            #print(merged_data)


            total_data = merged_data.drop(columns=[0])
            total_data.columns = ["class_name", "fault_flag", "suspicion"]
            total_data["rank"] = total_data.index
            total_data["percentile_rank"] = total_data.index / len(total_data)
            total_result_csv = pd.DataFrame(total_data.loc[total_data["fault_flag"] == 1])
            total_result_csv.to_csv(
                self.base_path + self.total_result_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv",
                index=False)
            print(
                "Create total data : ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")

    def generate_rank_data_metrics(self):
        # print("hello")
        if not os.path.exists(
                self.base_path + self.total_result_data_ccm_path +"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number):
            os.makedirs(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number)
        if not os.path.exists(
                self.base_path + self.total_result_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info"):
            os.makedirs(
                self.base_path + self.total_result_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info")
        faulty_info_path = self.base_path + "method_data/fault_data/existing_csv/"
        # file_numbers = [f.split("_")[-2] for f in os.listdir(base_path + "method_fault_info")]
        file_numbers = [f.split("_")[-2] for f in os.listdir(faulty_info_path)]
        # print(file_numbers)

        for file_number in file_numbers:
            file_number = str(file_number)

            # line_data_path = base_path + "stmt_data/stmt_" + self.project_name + "_" + file_number + "_data.csv"
            suspicion_data_path = self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + file_number + "/" + self.project_name + "_" + file_number + "_b_train_result_sample-" + str(
                self.sample_number) + "_step-" + self.step_number + "_nodes-" + self.node_number + ".csv"

            fault_info_path = self.base_path + "method_data/fault_data/existing_csv/method_fault_" + self.project_name + "_" + file_number + "_.csv"
            # fault_info_path = base_path + "patch_data_csv/method_fault_" + file_number + ".csv"

            try:
                # line_data = pd.read_csv(line_data_path, header=None)
                suspicion_data = pd.read_csv(suspicion_data_path, header=None)
                fault_data = pd.read_csv(fault_info_path, header=None)

            except (FileNotFoundError, pd.errors.EmptyDataError) as e:
                # print(file_number)
                print(e)
                continue
            # print(suspicion_data)
            #print(fault_data)
            # print(fault_info_path)
            # print(suspicion_data_path)
            if suspicion_data.isnull().any().any() == True :
                print("Not Read : ", suspicion_data_path)
                continue
            # print(suspicion_data)
            merged_data = pd.merge(fault_data, suspicion_data, how="left", on=0)
            merged_data = merged_data.sort_values(["1_y", 2], ascending=[False, False])
            merged_data = merged_data.reset_index(drop=True)

            total_data = merged_data.drop(columns=[0])
            total_data.columns = ["class_name", "fault_flag", "suspicion"]
            total_data["rank"] = total_data.index
            total_data["percentile_rank"] = total_data.index / len(total_data)
            total_result_csv = pd.DataFrame(total_data.loc[total_data["fault_flag"] == 1])
            total_data.to_csv(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info/average_all_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")
            total_result_csv.to_csv(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/average_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv",
                index=False)
            print(
                "Create total data using ccm_average : average_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")

    def generate_rank_data_metrics_min(self):
        # print("hello")
        if not os.path.exists(
                self.base_path + self.total_result_data_ccm_path +"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number):
            os.makedirs(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number)
        if not os.path.exists(
                self.base_path + self.total_result_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info"):
            os.makedirs(
                self.base_path + self.total_result_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info")
        faulty_info_path = self.base_path + "method_data/fault_data/existing_csv/"
        # file_numbers = [f.split("_")[-2] for f in os.listdir(base_path + "method_fault_info")]
        file_numbers = [f.split("_")[-2] for f in os.listdir(faulty_info_path)]
        # print(file_numbers)

        for file_number in file_numbers:
            file_number = str(file_number)

            # line_data_path = base_path + "stmt_data/stmt_" + self.project_name + "_" + file_number + "_data.csv"
            suspicion_data_path = self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + file_number + "/min_" + self.project_name + "_" + file_number + "_b_train_result_sample-" + str(
                self.sample_number) + "_step-" + self.step_number + "_nodes-" + self.node_number + ".csv"

            fault_info_path = self.base_path + "method_data/fault_data/existing_csv/method_fault_" + self.project_name + "_" + file_number + "_.csv"
            # fault_info_path = base_path + "patch_data_csv/method_fault_" + file_number + ".csv"

            try:
                # line_data = pd.read_csv(line_data_path, header=None)
                suspicion_data = pd.read_csv(suspicion_data_path, header=None)
                fault_data = pd.read_csv(fault_info_path, header=None)

            except (FileNotFoundError, pd.errors.EmptyDataError) as e:
                # print(file_number)
                print(e)
                continue
            # print(suspicion_data)
            #print(fault_data)
            # print(fault_info_path)
            # print(suspicion_data_path)
            if suspicion_data.isnull().any().any() == True :
                print("Not Read : ", suspicion_data_path)
                continue
            # print(suspicion_data)
            merged_data = pd.merge(fault_data, suspicion_data, how="left", on=0)
            merged_data = merged_data.sort_values(["1_y", 2], ascending=[False, False])
            merged_data = merged_data.reset_index(drop=True)

            total_data = merged_data.drop(columns=[0])
            total_data.columns = ["class_name", "fault_flag", "suspicion"]
            total_data["rank"] = total_data.index
            total_data["percentile_rank"] = total_data.index / len(total_data)
            total_result_csv = pd.DataFrame(total_data.loc[total_data["fault_flag"] == 1])
            total_data.to_csv(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info/min_all_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")
            total_result_csv.to_csv(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/min_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv",
                index=False)
            print(
                "Create total data using ccm_min : min_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")

    def generate_rank_data_metrics_max(self):
        # print("hello")
        if not os.path.exists(
                self.base_path + self.total_result_data_ccm_path +"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number):
            os.makedirs(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number)
        if not os.path.exists(
                self.base_path + self.total_result_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info"):
            os.makedirs(
                self.base_path + self.total_result_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info")
        faulty_info_path = self.base_path + "method_data/fault_data/existing_csv/"
        # file_numbers = [f.split("_")[-2] for f in os.listdir(base_path + "method_fault_info")]
        file_numbers = [f.split("_")[-2] for f in os.listdir(faulty_info_path)]
        # print(file_numbers)

        for file_number in file_numbers:
            file_number = str(file_number)

            # line_data_path = base_path + "stmt_data/stmt_" + self.project_name + "_" + file_number + "_data.csv"
            suspicion_data_path = self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + file_number + "/max_" + self.project_name + "_" + file_number + "_b_train_result_sample-" + str(
                self.sample_number) + "_step-" + self.step_number + "_nodes-" + self.node_number + ".csv"

            fault_info_path = self.base_path + "method_data/fault_data/existing_csv/method_fault_" + self.project_name + "_" + file_number + "_.csv"
            # fault_info_path = base_path + "patch_data_csv/method_fault_" + file_number + ".csv"

            try:
                # line_data = pd.read_csv(line_data_path, header=None)
                suspicion_data = pd.read_csv(suspicion_data_path, header=None)
                fault_data = pd.read_csv(fault_info_path, header=None)

            except (FileNotFoundError, pd.errors.EmptyDataError) as e:
                # print(file_number)
                print(e)
                continue
            # print(suspicion_data)
            #print(fault_data)
            # print(fault_info_path)
            # print(suspicion_data_path)
            if suspicion_data.isnull().any().any() == True :
                print("Not Read : ", suspicion_data_path)
                continue
            # print(suspicion_data)
            merged_data = pd.merge(fault_data, suspicion_data, how="left", on=0)
            merged_data = merged_data.sort_values(["1_y", 2], ascending=[False, False])
            merged_data = merged_data.reset_index(drop=True)

            total_data = merged_data.drop(columns=[0])
            total_data.columns = ["class_name", "fault_flag", "suspicion"]
            total_data["rank"] = total_data.index
            total_data["percentile_rank"] = total_data.index / len(total_data)
            total_result_csv = pd.DataFrame(total_data.loc[total_data["fault_flag"] == 1])
            total_data.to_csv(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/all_info/max_all_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")
            total_result_csv.to_csv(
                self.base_path + self.total_result_data_ccm_path+"sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/max_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv",
                index=False)
            print(
                "Create total data using ccm_max : max_ranked_" + self.project_name + "_" + file_number + "_sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".csv")


if __name__ == '__main__':
    p_name = "Time"

    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="100", node_number="500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="200", node_number="500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="300", node_number="500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="100", node_number="1000")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="200", node_number="1000")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="300", node_number="1000")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="100", node_number="1500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="200", node_number="1500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="500", step_num="300", node_number="1500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()

    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="100", node_number="500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="200", node_number="500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="300", node_number="500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="100", node_number="1000")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="200", node_number="1000")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="300", node_number="1000")
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="100", node_number="1500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="200", node_number="1500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()
    concatenator = Concatenator(project_name=p_name, sample_num="1000", step_num="300", node_number="1500")
    concatenator.generate_rank_data_v1()
    concatenator.generate_rank_data_metrics()
    concatenator.generate_rank_data_metrics_min()
    concatenator.generate_rank_data_metrics_max()

    # concatenator.generate_rank_data_metrics()
    # concatenator.generate_rank_data_v1()
