import os, sys, getopt
import pandas as pd
import numpy as np
from matplotlib.patches import Polygon
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
from os.path import isfile, join
from configurator import Configurator

class Drawer:
    def __init__(self, project_name=None, sample_num=None, step_num=None, node_number=None):
        self.project_name = project_name

        self.d4j_env = os.environ["D4J_HOME"]
        self.pwd = os.getcwd()
        #self.d4j_env = "/mnt/d/workdir/defects4j"
        # self.pwd = "/mnt/d/180603/Math"
        # self.d4j_env = "/mnt/d/workdir/defects4j"
        # self.pwd = "/mnt/d/180603/" + self.project_name
        #self.pwd = "/mnt/c/Data/" + self.project_name

        self.data_category = "/method_data/"
        self.stmt_path = "/framework/bin/fluccs/method_stmt/"
        self.sample_number = sample_num
        self.step_number = step_num
        self.node_number = node_number

        self.coverage_origin_path = "coverage_data/origin_data/"
        self.coverage_ccm_path = "coverage_data/ccm_data/"
        self.train_data_origin_path = "train_data/origin_data/"
        self.train_data_ccm_path = "train_data/ccm_data/"
        self.train_data_ccm_x_path = "train_data/ccm_x_data/"

        self.graph_data_origin_path = "total_data/graph_data/origin_data/"
        self.graph_data_ccm_path = "total_data/graph_data/ccm_data/"
        self.rank_data_origin_path = "total_data/rank_data/origin_data/"
        self.rank_data_ccm_path = "total_data/rank_data/ccm_data/"
        self.line_path = "method_data/line_data/"
        self.base_path = self.pwd + "/outputs/" + self.project_name + "/"

    ## TODO : Draw multiple graph

    def draw_result(self):

        if not os.path.exists(
                self.base_path + self.graph_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number):
            os.makedirs(
                self.base_path + self.graph_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number)

        files_location = self.base_path + self.rank_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
        file_name_list = [f for f in os.listdir(files_location) if isfile(join(files_location, f))]
        print(file_name_list)
        percentage_list = []
        rank_list = []

        for file_name in file_name_list:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list.append(csv_file["percentile_rank"][0])
                print("Fault :", file_name.split("_")[2], ", Rank : ", csv_file["rank"][0])
                rank_list.append(csv_file["rank"][0])
            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue

        percentage_list = list(map(lambda x: x * 100, (percentage_list)))
        x_axis_values = np.arange(0, 101, 1)
        percentage_list_len = len(percentage_list)
        y_axis_count = []
        y_axis_percentage = []
        x = np.array(rank_list)
        unique, counts = np.unique(x, return_counts=True)
        print(unique + 1, counts)

        str_x_value = []
        for x_value in x_axis_values:
            if (x_value % 10) == 0:
                str_x_value.append(str(x_value) + "%")

            result_list = np.where(percentage_list <= x_value)
            y_axis_count.append(len(list(result_list[0])))
            y_axis_percentage.append(len(list(result_list[0])) / percentage_list_len)
        y_percent_list = list(map(lambda x: x * 100, (y_axis_percentage)))

        loc_10 = plticker.MultipleLocator(base=10.0)
        loc_5 = plticker.MultipleLocator(base=5.0)

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, len(percentage_list)])
        print(len(percentage_list))
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        ##Strings
        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_axis_count)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_origin_path + "versions_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, 101])
        ax.xaxis.set_major_locator(loc_5)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "% of faulty versions"

        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_percent_list)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_origin_path + "percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

    def draw_result_v1(self):
        if not os.path.exists(self.base_path + self.graph_data_ccm_path):
            os.makedirs(self.base_path + self.graph_data_ccm_path)

        files_location = self.base_path + self.rank_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
        file_name_list = [f for f in os.listdir(files_location) if isfile(join(files_location, f))]
        percentage_list = []
        rank_list = []
        for file_name in file_name_list:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list.append(csv_file["percentile_rank"][0])
                print("Fault num :", file_name.split("_")[2], "Rank num : ", csv_file["rank"][0])
                rank_list.append(csv_file["rank"][0])

            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue
        percentage_list = list(map(lambda x: x * 100, (percentage_list)))
        x_axis_values = np.arange(0, 101, 1)
        percentage_list_len = len(percentage_list)
        y_axis_count = []
        y_axis_percentage = []
        x = np.array(rank_list)
        unique, counts = np.unique(x, return_counts=True)
        keys = unique + 1
        values = counts
        top_dict = dict(zip(keys, values))
        print(top_dict)
        print(keys, values)

        plt.plot(unique, counts, label="hello")
        plt.xlabel("top")
        plt.ylabel("count")
        plt.title("Plot")
        plt.show()

        str_x_value = []
        for x_value in x_axis_values:
            if (x_value % 10) == 0:
                str_x_value.append(str(x_value) + "%")
            result_list = np.where(percentage_list <= x_value)
            y_axis_count.append(len(list(result_list[0])))
            # print(y_axis_count)
            y_axis_percentage.append(len(list(result_list[0])) / percentage_list_len)

        y_percent_list = list(map(lambda x: x * 100, (y_axis_percentage)))

        loc_10 = plticker.MultipleLocator(base=10.0)
        loc_5 = plticker.MultipleLocator(base=5.0)
        #
        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, len(percentage_list)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        # print(x_axis_values)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_axis_count)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "versions_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, 101])
        ax.xaxis.set_major_locator(loc_5)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "% of faulty versions"

        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_percent_list)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")

        # fig.savefig(base_path + "metrics_graph_data/sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 20])
        ax.set_ylim([0, max(counts)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(unique, counts)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "histogram_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

    def draw_result_metrics(self):

        if not os.path.exists(self.base_path + self.graph_data_ccm_path):
            os.makedirs(self.base_path + self.graph_data_ccm_path)

        files_location = self.base_path + self.rank_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
        file_name_list = [f for f in os.listdir(files_location) if isfile(join(files_location, f))]
        file_name_list = [f for f in file_name_list if "average_" in f]

        percentage_list = []
        rank_list = []
        for file_name in file_name_list:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list.append(csv_file["percentile_rank"][0])
                print("Fault num :", file_name.split("_")[2], ", Rank num : ", csv_file["rank"][0])
                rank_list.append(csv_file["rank"][0])

            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue
        percentage_list = list(map(lambda x: x * 100, (percentage_list)))
        x_axis_values = np.arange(0, 101, 1)
        percentage_list_len = len(percentage_list)
        y_axis_count = []
        y_axis_percentage = []
        x = np.array(rank_list)
        unique, counts = np.unique(x, return_counts=True)
        keys = unique + 1
        values = counts
        # top_dict = dict(zip(keys, values))
        # print(top_dict)
        print(keys, values)

        # print(unique+1, counts)

        plt.plot(unique, counts, label="hello")
        plt.xlabel("top")
        plt.ylabel("count")
        plt.title("Plot")
        plt.show()

        str_x_value = []
        for x_value in x_axis_values:
            if (x_value % 10) == 0:
                str_x_value.append(str(x_value) + "%")
            result_list = np.where(percentage_list <= x_value)
            y_axis_count.append(len(list(result_list[0])))
            # print(y_axis_count)
            y_axis_percentage.append(len(list(result_list[0])) / percentage_list_len)

        y_percent_list = list(map(lambda x: x * 100, (y_axis_percentage)))

        loc_10 = plticker.MultipleLocator(base=10.0)
        loc_5 = plticker.MultipleLocator(base=5.0)
        #
        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, len(percentage_list)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        # print(x_axis_values)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_axis_count)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "versions_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, 101])
        ax.xaxis.set_major_locator(loc_5)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "% of faulty versions"

        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_percent_list)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")

        # fig.savefig(base_path + "metrics_graph_data/sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 20])
        ax.set_ylim([0, max(counts)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(unique, counts)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "histogram_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

    def draw_result_metrics_min(self):

        if not os.path.exists(self.base_path + self.graph_data_ccm_path):
            os.makedirs(self.base_path + self.graph_data_ccm_path)

        files_location = self.base_path + self.rank_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
        file_name_list = [f for f in os.listdir(files_location) if isfile(join(files_location, f))]
        file_name_list = [f for f in file_name_list if "min_" in f]

        percentage_list = []
        rank_list = []
        for file_name in file_name_list:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list.append(csv_file["percentile_rank"][0])
                print("Fault num :", file_name.split("_")[2], ", Rank num : ", csv_file["rank"][0])
                rank_list.append(csv_file["rank"][0])

            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue
        percentage_list = list(map(lambda x: x * 100, (percentage_list)))
        x_axis_values = np.arange(0, 101, 1)
        percentage_list_len = len(percentage_list)
        y_axis_count = []
        y_axis_percentage = []
        x = np.array(rank_list)
        unique, counts = np.unique(x, return_counts=True)
        keys = unique + 1
        values = counts
        # top_dict = dict(zip(keys, values))
        # print(top_dict)
        print(keys, values)

        # print(unique+1, counts)

        plt.plot(unique, counts, label="hello")
        plt.xlabel("top")
        plt.ylabel("count")
        plt.title("Plot")
        plt.show()

        str_x_value = []
        for x_value in x_axis_values:
            if (x_value % 10) == 0:
                str_x_value.append(str(x_value) + "%")
            result_list = np.where(percentage_list <= x_value)
            y_axis_count.append(len(list(result_list[0])))
            # print(y_axis_count)
            y_axis_percentage.append(len(list(result_list[0])) / percentage_list_len)

        y_percent_list = list(map(lambda x: x * 100, (y_axis_percentage)))

        loc_10 = plticker.MultipleLocator(base=10.0)
        loc_5 = plticker.MultipleLocator(base=5.0)
        #
        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, len(percentage_list)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        # print(x_axis_values)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_axis_count)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "min_versions_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, 101])
        ax.xaxis.set_major_locator(loc_5)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "% of faulty versions"

        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_percent_list)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "min_percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")

        # fig.savefig(base_path + "metrics_graph_data/sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 20])
        ax.set_ylim([0, max(counts)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(unique, counts)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "min_histogram_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

    def draw_result_metrics_max(self):

        if not os.path.exists(self.base_path + self.graph_data_ccm_path):
            os.makedirs(self.base_path + self.graph_data_ccm_path)

        files_location = self.base_path + self.rank_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
        file_name_list = [f for f in os.listdir(files_location) if isfile(join(files_location, f))]
        file_name_list = [f for f in file_name_list if "max_" in f]
        percentage_list = []
        rank_list = []
        for file_name in file_name_list:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list.append(csv_file["percentile_rank"][0])
                print("Fault num :", file_name.split("_")[2], ", Rank num : ", csv_file["rank"][0])
                rank_list.append(csv_file["rank"][0])

            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue
        percentage_list = list(map(lambda x: x * 100, (percentage_list)))
        x_axis_values = np.arange(0, 101, 1)
        percentage_list_len = len(percentage_list)
        y_axis_count = []
        y_axis_percentage = []
        x = np.array(rank_list)
        unique, counts = np.unique(x, return_counts=True)
        keys = unique + 1
        values = counts
        # top_dict = dict(zip(keys, values))
        # print(top_dict)
        print(keys, values)

        # print(unique+1, counts)

        plt.plot(unique, counts, label="hello")
        plt.xlabel("top")
        plt.ylabel("count")
        plt.title("Plot")
        plt.show()

        str_x_value = []
        for x_value in x_axis_values:
            if (x_value % 10) == 0:
                str_x_value.append(str(x_value) + "%")
            result_list = np.where(percentage_list <= x_value)
            y_axis_count.append(len(list(result_list[0])))
            # print(y_axis_count)
            y_axis_percentage.append(len(list(result_list[0])) / percentage_list_len)

        y_percent_list = list(map(lambda x: x * 100, (y_axis_percentage)))

        loc_10 = plticker.MultipleLocator(base=10.0)
        loc_5 = plticker.MultipleLocator(base=5.0)
        #
        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, len(percentage_list)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        # print(x_axis_values)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_axis_count)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "max_versions_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, 101])
        ax.xaxis.set_major_locator(loc_5)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "% of faulty versions"

        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(x_axis_values, y_percent_list)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "max_percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")

        # fig.savefig(base_path + "metrics_graph_data/sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 20])
        ax.set_ylim([0, max(counts)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.plot(unique, counts)
        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "max_histogram_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

    def draw_result_metrics_total(self):

        percentage_list_max = []

        percentage_list_min = []
        percentage_list_average = []
        percentage_list_origin = []

        max_rank_list = []
        min_rank_list = []
        average_rank_list = []
        origin_rank_list = []

        if not os.path.exists(self.base_path + self.graph_data_ccm_path):
            os.makedirs(self.base_path + self.graph_data_ccm_path)

        ## read origin data
        files_location_origin = self.base_path + self.rank_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
        file_name_origin = [f for f in os.listdir(files_location_origin) if isfile(join(files_location_origin, f))]

        for file_name in file_name_origin:
            csv_file = pd.read_csv(files_location_origin + file_name)
            try:
                percentage_list_origin.append(csv_file["percentile_rank"][0])
                print("Fault :", file_name.split("_")[2], ", Rank : ", csv_file["rank"][0])
                origin_rank_list.append(csv_file["rank"][0])
            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue

        percentage_list_origin = list(map(lambda x: x * 100, (percentage_list_origin)))
        x_axis_values = np.arange(0, 101, 1)
        percentage_list_len = len(percentage_list_origin)
        y_axis_count_origin = []
        y_axis_percentage_origin = []

        str_x_value = []
        for x_value in x_axis_values:
            if (x_value % 10) == 0:
                str_x_value.append(str(x_value) + "%")

            result_list = np.where(percentage_list_origin <= x_value)
            y_axis_count_origin.append(len(list(result_list[0])))
            y_axis_percentage_origin.append(len(list(result_list[0])) / percentage_list_len)
        y_percent_list_origin = list(map(lambda x: x * 100, (y_axis_percentage_origin)))

        ## read ccm data

        files_location = self.base_path + self.rank_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
        file_names_ccm = [f for f in os.listdir(files_location) if isfile(join(files_location, f))]

        ### file names
        max_file_names = [f for f in file_names_ccm if "max_" in f]
        min_file_names = [f for f in file_names_ccm if "min_" in f]
        average_file_names = [f for f in file_names_ccm if "average_" in f]

        ### Read data
        for file_name in max_file_names:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list_max.append(csv_file["percentile_rank"][0])
                print("Fault num :", file_name.split("_")[2], ", Rank num : ", csv_file["rank"][0])
                max_rank_list.append(csv_file["rank"][0])

            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue

        for file_name in min_file_names:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list_min.append(csv_file["percentile_rank"][0])
                print("Fault num :", file_name.split("_")[2], ", Rank num : ", csv_file["rank"][0])
                min_rank_list.append(csv_file["rank"][0])

            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue

        for file_name in average_file_names:
            csv_file = pd.read_csv(files_location + file_name)
            try:
                percentage_list_average.append(csv_file["percentile_rank"][0])
                print("Fault num :", file_name.split("_")[2], ", Rank num : ", csv_file["rank"][0])
                average_rank_list.append(csv_file["rank"][0])

            except (IndexError, FileNotFoundError) as e:
                print("file_name : ", file_name, "error msg : ", e)
                continue

        # x_max = np.array(max_rank_list)
        # x_min = np.array(min_rank_list)
        # x_average = np.array(average_rank_list)

        percentage_list_max = list(map(lambda x: x * 100, (percentage_list_max)))
        percentage_list_len_max = len(percentage_list_max)

        percentage_list_min = list(map(lambda x: x * 100, (percentage_list_min)))
        percentage_list_len_min = len(percentage_list_min)

        percentage_list_average = list(map(lambda x: x * 100, (percentage_list_average)))
        percentage_list_len_average = len(percentage_list_average)

        x_axis_values = np.arange(0, 101, 1)
        y_axis_count_max = []
        y_axis_count_min = []
        y_axis_count_average = []

        y_axis_percentage_max = []
        y_axis_percentage_min = []
        y_axis_percentage_average = []

        for x_value in x_axis_values:
            result_list_max = np.where(percentage_list_max <= x_value)
            y_axis_count_max.append(len(list(result_list_max[0])))
            y_axis_percentage_max.append(len(list(result_list_max[0])) / percentage_list_len_max)

            result_list_min = np.where(percentage_list_min <= x_value)
            y_axis_count_min.append(len(list(result_list_min[0])))
            y_axis_percentage_min.append(len(list(result_list_min[0])) / percentage_list_len_min)

            result_list_average = np.where(percentage_list_average <= x_value)
            y_axis_count_average.append(len(list(result_list_average[0])))
            y_axis_percentage_average.append(len(list(result_list_average[0])) / percentage_list_len_average)

        y_percent_list_max = list(map(lambda x: x * 100, (y_axis_percentage_max)))
        y_percent_list_min = list(map(lambda x: x * 100, (y_axis_percentage_min)))
        y_percent_list_average = list(map(lambda x: x * 100, (y_axis_percentage_average)))

        loc_10 = plticker.MultipleLocator(base=10.0)
        loc_5 = plticker.MultipleLocator(base=5.0)

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, len(percentage_list_max)])
        ax.xaxis.set_major_locator(loc_10)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "faulty versions"
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        print(y_axis_count_max)
        print(y_axis_count_min)
        print(y_axis_count_average)
        print(y_axis_count_origin)
        plt.plot(x_axis_values, y_axis_count_max, 'r--', x_axis_values, y_axis_count_min, 'b--', x_axis_values, y_axis_count_average, 'g--', x_axis_values, y_axis_count_origin, 'k--')
        # plt.plot(x_axis_values, y_axis_count_max, 'r--', x_axis_values, y_axis_count_min, 'b--', x_axis_values, y_axis_count_average, 'g--', x_axis_values, y_axis_count_origin, 'k--')

        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "total_versions_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0, 101])
        ax.set_ylim([0, 101])
        ax.xaxis.set_major_locator(loc_5)
        ax.yaxis.set_major_locator(loc_5)

        x_label = "% of executable methods that are examined"
        y_label = "% of faulty versions"

        plt.xlabel(x_label)
        plt.ylabel(y_label)
        # plt.plot(x_axis_values, y_percent_list_max, 'r--', x_axis_values, y_percent_list_min, 'b--', x_axis_values, y_percent_list_average, 'g--', x_axis_values, y_percent_list_origin, 'k--')
        plt.plot(x_axis_values, y_percent_list_max, 'r--', x_axis_values, y_percent_list_min, 'b--', x_axis_values, y_percent_list_average, 'g--', x_axis_values, y_percent_list_origin, 'k--')

        plt.grid(True)
        fig.savefig(
            self.base_path + self.graph_data_ccm_path + "total_percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
        plt.close()

    def draw_result_metrics_total_v2(self):
            percentage_list_max = []
            percentage_list_min = []
            percentage_list_average = []
            percentage_list_origin = []
            max_rank_list = []
            min_rank_list = []
            average_rank_list = []
            origin_rank_list = []

            if not os.path.exists(self.base_path + self.graph_data_ccm_path):
                os.makedirs(self.base_path + self.graph_data_ccm_path)

            ## read origin data
            files_location_origin = self.base_path + self.rank_data_origin_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
            file_name_origin = [f for f in os.listdir(files_location_origin) if isfile(join(files_location_origin, f))]

            for file_name in file_name_origin:
                csv_file = pd.read_csv(files_location_origin + file_name)
                try:
                    percentage_list_origin.append(csv_file["percentile_rank"][0])
                    print("original data Fault Number," + str(file_name.split("_")[2]) + ",Rank Number," + str(csv_file["rank"][0]))
                    origin_rank_list.append(csv_file["rank"][0])
                except (IndexError, FileNotFoundError) as e:
                    print("file_name : ", file_name, "error msg : ", e)
                    continue

            percentage_list_origin = list(map(lambda x: x * 100, (percentage_list_origin)))
            x_axis_values = np.arange(0, 101, 1)
            percentage_list_len = len(percentage_list_origin)
            y_axis_count_origin = []
            y_axis_percentage_origin = []

            str_x_value = []
            for x_value in x_axis_values:
                if (x_value % 10) == 0:
                    str_x_value.append(str(x_value) + "%")

                result_list = np.where(percentage_list_origin <= x_value)
                y_axis_count_origin.append(len(list(result_list[0])))
                y_axis_percentage_origin.append(len(list(result_list[0])) / percentage_list_len)
            y_percent_list_origin = list(map(lambda x: x * 100, (y_axis_percentage_origin)))

            ## read ccm data

            files_location = self.base_path + self.rank_data_ccm_path + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + "/"
            file_names_ccm = [f for f in os.listdir(files_location) if isfile(join(files_location, f))]

            ### file names
            max_file_names = [f for f in file_names_ccm if "max_" in f]
            min_file_names = [f for f in file_names_ccm if "min_" in f]
            average_file_names = [f for f in file_names_ccm if "average_" in f]

            ### Read data
            for file_name in average_file_names:
                csv_file = pd.read_csv(files_location + file_name)
                try:
                    percentage_list_average.append(csv_file["percentile_rank"][0])
                    print("ccm avg data  Fault Number," + str(file_name.split("_")[3]) + ",Rank Number," + str(csv_file["rank"][0]))
                    average_rank_list.append(csv_file["rank"][0])

                except (IndexError, FileNotFoundError) as e:
                    print("file_name : ", file_name, "error msg : ", e)
                    continue


            for file_name in min_file_names:
                csv_file = pd.read_csv(files_location + file_name)
                try:
                    percentage_list_min.append(csv_file["percentile_rank"][0])
                    print("ccm min data Fault Number," + str(file_name.split("_")[3]) + ",Rank Number," + str(csv_file["rank"][0]))
                    min_rank_list.append(csv_file["rank"][0])

                except (IndexError, FileNotFoundError) as e:
                    print("file_name : ", file_name, "error msg : ", e)
                    continue

            for file_name in max_file_names:
                csv_file = pd.read_csv(files_location + file_name)
                try:
                    percentage_list_max.append(csv_file["percentile_rank"][0])
                    # print(file_name)
                    print("ccm max data Fault Number," + str(file_name.split("_")[3]) + ",Rank Number," + str(csv_file["rank"][0]))
                    max_rank_list.append(csv_file["rank"][0])

                except (IndexError, FileNotFoundError) as e:
                    print("file_name : ", file_name, "error msg : ", e)
                    continue



            percentage_list_max = list(map(lambda x: x * 100, (percentage_list_max)))
            percentage_list_len_max = len(percentage_list_max)

            percentage_list_min = list(map(lambda x: x * 100, (percentage_list_min)))
            percentage_list_len_min = len(percentage_list_min)

            percentage_list_average = list(map(lambda x: x * 100, (percentage_list_average)))
            percentage_list_len_average = len(percentage_list_average)

            x_axis_values = np.arange(0, 101, 1)
            y_axis_count_max = []
            y_axis_count_min = []
            y_axis_count_average = []

            y_axis_percentage_max = []
            y_axis_percentage_min = []
            y_axis_percentage_average = []

            data_to_plot = [percentage_list_origin, percentage_list_min, percentage_list_max, percentage_list_average]
            print(len(percentage_list_origin))
            print(np.mean(percentage_list_origin), np.mean(percentage_list_min), np.mean(percentage_list_max), np.mean(percentage_list_average))
            # Create a figure instance
            fig = plt.figure(1, figsize=(9, 6))

            # Create an axes instance
            ax = fig.add_subplot(111)
            ax.set_ylim([0, 100])
            ax.yaxis.set_major_locator(plticker.MultipleLocator(base=5.0))



            # Create the boxplot
            bp = ax.boxplot(data_to_plot, showmeans=True)

            # Save the figure
            # fig.savefig('fig1.png', bbox_inches='tight')
            fig.savefig(
                self.base_path + self.graph_data_ccm_path + "total_boxplot_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")

            fig.show()
            plt.close()

            # fig.close()

            for x_value in x_axis_values:
                result_list_max = np.where(percentage_list_max <= x_value)
                y_axis_count_max.append(len(list(result_list_max[0])))
                y_axis_percentage_max.append(len(list(result_list_max[0])) / percentage_list_len_max)

                result_list_min = np.where(percentage_list_min <= x_value)
                y_axis_count_min.append(len(list(result_list_min[0])))
                y_axis_percentage_min.append(len(list(result_list_min[0])) / percentage_list_len_min)

                result_list_average = np.where(percentage_list_average <= x_value)
                y_axis_count_average.append(len(list(result_list_average[0])))
                y_axis_percentage_average.append(len(list(result_list_average[0])) / percentage_list_len_average)

            y_percent_list_max = list(map(lambda x: x * 100, (y_axis_percentage_max)))
            y_percent_list_min = list(map(lambda x: x * 100, (y_axis_percentage_min)))
            y_percent_list_average = list(map(lambda x: x * 100, (y_axis_percentage_average)))

            loc_10 = plticker.MultipleLocator(base=10.0)
            loc_5 = plticker.MultipleLocator(base=5.0)

            fig = plt.figure(figsize=(13, 9))
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlim([0, 100])
            ax.set_ylim([0, len(percentage_list_max)])
            ax.xaxis.set_major_locator(loc_10)
            ax.yaxis.set_major_locator(loc_5)
            ax.plot(x_axis_values, y_axis_count_max, 'r--', label="Add to Change Metrics(Max)")
            ax.plot(x_axis_values, y_axis_count_min, 'b--', label="Add to Change Metrics(Min)")
            ax.plot(x_axis_values, y_axis_count_average, 'g--', label="Add to Change Metrics(Average)")
            ax.plot(x_axis_values, y_axis_count_origin, 'k--', label="Original")
            plt.xlabel("% of executable methods that are examined")
            plt.ylabel("Faulty versions")
            plt.title(self.project_name + " Subject Program")
            plt.grid(True)
            ax.legend(loc='upper center', bbox_to_anchor=(0.5, 0.1), shadow=True, ncol=2)
            fig.savefig(self.base_path + self.graph_data_ccm_path + "total_versions_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
            plt.close()


            fig = plt.figure(figsize=(13, 9))
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlim([0, 100])
            ax.set_ylim([0, 100])
            ax.xaxis.set_major_locator(loc_5)
            ax.yaxis.set_major_locator(loc_5)
            ax.plot(x_axis_values, y_percent_list_max, 'r--', label="Add to Change Metrics(Max)")
            ax.plot(x_axis_values, y_percent_list_min, 'b--', label="Add to Change Metrics(Min)")
            ax.plot(x_axis_values, y_percent_list_average, 'g--', label="Add to Change Metrics(Average)")
            ax.plot(x_axis_values, y_percent_list_origin, 'k--', label="Original")
            plt.xlabel("% of executable methods that are examined")
            plt.ylabel("% of faulty versions")
            plt.title(self.project_name + " Subject Program")
            plt.grid(True)
            ax.legend(loc='upper center', bbox_to_anchor=(0.5, 0.1), shadow=True, ncol=2)
            # plt.plot(x_axis_values, y_percent_list_max, 'r--', x_axis_values, y_percent_list_min, 'b--', x_axis_values, y_percent_list_average, 'g--', x_axis_values, y_percent_list_origin, 'k--')
            fig.show()
            fig.savefig(
                self.base_path + self.graph_data_ccm_path + "total_percentile_" + self.project_name + "_" + "sample-" + self.sample_number + "_step-" + self.step_number + "_node-" + self.node_number + ".png")
            plt.close()

    def draw_box_plot(self):


        # np.random.seed(19680801)

        # fake up some data
        spread = np.random.rand(50) * 100
        center = np.ones(25) * 50
        flier_high = np.random.rand(10) * 100 + 100
        flier_low = np.random.rand(10) * -100
        data = np.concatenate((spread, center, flier_high, flier_low), 0)

        fig, axs = plt.subplots(2, 3)

        # basic plot
        axs[0, 0].boxplot(data)
        axs[0, 0].set_title('basic plot')

        # notched plot
        axs[0, 1].boxplot(data, 1)
        axs[0, 1].set_title('notched plot')

        # change outlier point symbols
        axs[0, 2].boxplot(data, 0, 'gD')
        axs[0, 2].set_title('change outlier\npoint symbols')

        # don't show outlier points
        axs[1, 0].boxplot(data, 0, '')
        axs[1, 0].set_title("don't show\noutlier points")

        # horizontal boxes
        axs[1, 1].boxplot(data, 0, 'rs', 0)
        axs[1, 1].set_title('horizontal boxes')

        # change whisker length
        axs[1, 2].boxplot(data, 0, 'rs', 0, 0.75)
        axs[1, 2].set_title('change whisker length')

        fig.subplots_adjust(left=0.08, right=0.98, bottom=0.05, top=0.9,
                            hspace=0.4, wspace=0.3)

        # fake up some more data
        spread = np.random.rand(50) * 100
        center = np.ones(25) * 40
        flier_high = np.random.rand(10) * 100 + 100
        flier_low = np.random.rand(10) * -100
        d2 = np.concatenate((spread, center, flier_high, flier_low), 0)
        data.shape = (-1, 1)
        d2.shape = (-1, 1)
        # Making a 2-D array only works if all the columns are the
        # same length.  If they are not, then use a list instead.
        # This is actually more efficient because boxplot converts
        # a 2-D array into a list of vectors internally anyway.
        data = [data, d2, d2[::2, 0]]

        # Multiple box plots on one Axes
        fig, ax = plt.subplots()
        ax.boxplot(data)

        plt.show()

    def draw2(self):
        percentage_list_max = []
        percentage_list_min = []
        percentage_list_average = []
        percentage_list_origin = []

        # np.random.seed(10)
        collectn_1 = np.random.normal(100, 10, 200)
        collectn_2 = np.random.normal(80, 30, 200)
        collectn_3 = np.random.normal(90, 20, 200)
        collectn_4 = np.random.normal(70, 25, 200)
        print(collectn_1.shape)
        ## combine these different collections into a list
        data_to_plot = [collectn_1, collectn_2, collectn_3, collectn_4]

        # Create a figure instance
        fig = plt.figure(1, figsize=(9, 6))

        # Create an axes instance
        ax = fig.add_subplot(111)

        # Create the boxplot
        bp = ax.boxplot(data_to_plot)

        # Save the figure
        # fig.savefig('fig1.png', bbox_inches='tight')
        fig.show()



if __name__ == '__main__':
    p_name = "Lang"
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="100", node_number="500")
    # drawer.draw2()
    #drawer = Drawer(project_name=p_name, sample_num="500", step_num="100", node_number="500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="200", node_number="500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="300", node_number="500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="100", node_number="1000")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="200", node_number="1000")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="300", node_number="1000")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="100", node_number="1500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="200", node_number="1500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="500", step_num="300", node_number="1500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="100", node_number="500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="200", node_number="500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="300", node_number="500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="100", node_number="1000")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="200", node_number="1000")
    # drawer.draw_result_metrics_total_v2()
    drawer = Drawer(project_name=p_name, sample_num="1000", step_num="300", node_number="500")
    drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="100", node_number="1500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="200", node_number="1500")
    # drawer.draw_result_metrics_total_v2()
    # drawer = Drawer(project_name=p_name, sample_num="1000", step_num="300", node_number="1500")
    # drawer.draw_result_metrics_total_v2()



