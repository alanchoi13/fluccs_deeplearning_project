import sys, os, getopt


class Configurator:
    def __init__(self):
        self.project_name = None
        self.faulty_version = None
        self.working_directory = None
        self.sample_number = None
        self.epoch_number = None
        self.argv_list = []
        self.option_name = None
        self.node_number = None

    def set_argv(self, argv):
        self.check_args(argv)

        if argv[0] == "gen_coverage_data":
            self.get_opts_pvw(argv)

            self.check_project_name()

            self.check_faulty_version()

            self.check_working_dir()

            return [self.option_name, self.project_name, self.faulty_version, self.working_directory]

        elif argv[0] == "gen_coverage_ccm_data" or \
                argv[0] == "gen_coverage_ccm_data_min" or \
                argv[0] == "gen_coverage_ccm_data_max" or \
                argv[0] == 'gen_fault_data':

            self.get_opts_p(argv)

            self.check_project_name()

            return [self.option_name, self.project_name]

        elif argv[0] == "gen_train_data" or \
                argv[0] == "gen_train_ccm_data" or \
                argv[0] == "gen_train_ccm_data_min" or \
                argv[0] == "gen_train_ccm_data_max":

            self.get_opts_pvs(argv)

            self.check_project_name()

            self.check_faulty_version()

            self.check_sample_number()

            # print(self.project_name, self.faulty_version, self.sample_number)

            return [self.option_name, self.project_name, self.faulty_version, self.sample_number]

        elif argv[0] == "train_data" or \
                argv[0] == 'train_ccm_data' or \
                argv[0] == 'train_ccm_data_min' or \
                argv[0] == 'train_ccm_data_max':
            self.get_opts_pvsn(argv)

            self.check_project_name()

            self.check_faulty_version()

            self.check_sample_number()

            self.check_node_number()

            return [self.option_name, self.project_name, self.faulty_version, self.sample_number, self.node_number]

        elif argv[0] == "gen_result_data" or \
                argv[0] == 'gen_result_ccm_data':
            self.get_opts_psen(argv)

            self.check_project_name()

            self.check_sample_number()

            self.check_epoch_number()

            self.check_node_number()

            return [self.option_name, self.project_name, self.sample_number, self.epoch_number, self.node_number]

    def check_args(self, argv):
        options = ["gen_coverage_data",
                   "gen_coverage_ccm_data",
                   "gen_coverage_ccm_data_min",
                   "gen_coverage_ccm_data_max",
                   "gen_train_data",
                   "gen_train_ccm_data",
                   "gen_train_ccm_data_min",
                   "gen_train_ccm_data_max",
                   "train_data",
                   "train_ccm_data",
                   "train_ccm_data_min",
                   "train_ccm_data_max",
                   "gen_fault_data",
                   "gen_result_data",
                   "gen_result_ccm_data"]
        try:
            if not argv[0] in options:
                print(
                    "First argument is one of "
                    "\n== Generate coverage data =="
                    "\ngen_coverage_data"
                    "\ngen_coverage_ccm_data"
                    "\ngen_coverage_ccm_data_min"
                    "\ngen_coverage_ccm_data_max"
                    "\n"
                    "\n== Generate train data =="
                    "\ngen_train_data"
                    "\ngen_train_ccm_data"
                    "\ngen_train_ccm_data_min"
                    "\ngen_train_ccm_data_max"
                    "\n"
                    "\n== Train data =="
                    "\ntrain_data"
                    "\ntrain_ccm_data"
                    "\ntrain_ccm_data_min"
                    "\ntrain_ccm_data_max"
                    "\n"
                    "\n== Generate fault data =="
                    "\ngen_fault_data"
                    "\n"
                    "\n== Generate result data =="
                    "\ngen_result_data"
                    "\ngen_result_ccm_data"
                    "\n")
                sys.exit(1)
        except IndexError:
            print(
                "First argument is one of "
                "\n== Generate coverage data =="
                "\ngen_coverage_data"
                "\ngen_coverage_ccm_data"
                "\ngen_coverage_ccm_data_min"
                "\ngen_coverage_ccm_data_max"
                "\n"
                "\n== Generate train data =="
                "\ngen_train_data"
                "\ngen_train_ccm_data"
                "\ngen_train_ccm_data_min"
                "\ngen_train_ccm_data_max"
                "\n"
                "\n== Train data =="
                "\ntrain_data"
                "\ntrain_ccm_data"
                "\ntrain_ccm_data_min"
                "\ntrain_ccm_data_max"
                "\n"
                "\n== Generate fault data =="
                "\ngen_fault_data"
                "\n"
                "\n== Generate result data =="
                "\ngen_result_data"
                "\ngen_result_ccm_data"
                "\ngen_result_ccm_min"
                "\ngen_result_ccm_max"
                "\n")
            sys.exit(1)

    def check_epoch_number(self):
        if self.epoch_number is None:
            print("Please input argv -e(epoch_number) ex) -e [100|200|300|...|800|900|1000]")
            sys.exit(1)

    def get_opts_psen(self, argv):
        self.option_name = argv[0]
        try:
            opts, args = getopt.getopt(argv[1:], "p:s:e:n:", ["project=", "sample=", "epoch=", "nodes="])
        except getopt.getopterror as err:
            print(
                "Check argv convention ex) -p [Chart|Closure|Lang|Math|Mockito|Time] (project name) -v 1 (faulty version) -s 1000 (sample_number) ")
            sys.exit(1)
        for opt, arg in opts:
            if opt == "-p":
                self.project_name = arg
            elif opt == "-s":
                self.sample_number = arg
            elif opt == "-e":
                self.epoch_number = arg
            elif opt == "-n":
                self.node_number = arg

    def get_opts_p(self, argv):
        self.option_name = argv[0]
        try:
            opts, args = getopt.getopt(argv[1:], "p:", ["project="])
        except getopt.getopterror as err:
            sys.exit(1)
        for opt, arg in opts:
            if opt == "-p":
                self.project_name = arg

    def check_node_number(self):
        if self.node_number is None:
            print("Please input argv -n(node_number) ex) -n [1000|1500|2000|...|]")
            sys.exit(1)

    def get_opts_pvsn(self, argv):
        self.option_name = argv[0]
        try:
            opts, args = getopt.getopt(argv[1:], "p:v:s:n:", ["project=", "version=", "sample=", "node="])
        except getopt.getopterror as err:
            sys.exit(1)
        for opt, arg in opts:
            if opt == "-p":
                self.project_name = arg
            elif opt == "-v":
                self.faulty_version = arg
            elif opt == "-s":
                self.sample_number = arg
            elif opt == "-n":
                self.node_number = arg

    def get_opts_pvs(self, argv):
        self.option_name = argv[0]
        try:
            opts, args = getopt.getopt(argv[1:], "p:v:s:", ["project=", "version=", "sample="])
        except (getopt.getopterror, AttributeError, getopt.GetoptError) as err:
            sys.exit(1)
        for opt, arg in opts:
            if opt == "-p":
                self.project_name = arg
            elif opt == "-v":
                self.faulty_version = arg
            elif opt == "-s":
                self.sample_number = arg

    def get_opts_pvw(self, argv):
        self.option_name = argv[0]
        try:
            opts, args = getopt.getopt(argv[1:], "p:v:w:", ["project=", "version=", "workdir="])
        except (getopt.getopterror, AttributeError, getopt.GetoptError) as err:
            sys.exit(1)
        for opt, arg in opts:
            if opt == "-p":
                self.project_name = arg
            elif opt == "-v":
                self.faulty_version = arg
            elif opt == "-w":
                self.working_directory = arg

    def check_working_dir(self):
        if self.working_directory is None:
            print("Please input argv -w(working_dir) ex) -w closure_1_b")
            sys.exit(1)

    def check_sample_number(self):
        if self.sample_number is None:
            print("Please input argv -s(sample_number) ex) -s 1000")
            sys.exit(1)

    def check_faulty_version(self):
        if self.faulty_version is None:
            print("Please input argv -v(faulty_version) ex) -v 1")
            sys.exit(1)

    def check_project_name(self):
        if self.project_name is None:
            print("Please input argv -p(project_name) ex) -p [Chart|Closure|Lang|Math|Mockito|Time]")
            sys.exit(1)

    def get_argv(self):
        return [self.project_name, self.faulty_version, self.working_directory]


if __name__ == '__main__':
    configurator = Configurator()
    configurator.set_argv(sys.argv[1:])
