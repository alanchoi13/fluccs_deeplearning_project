import sys, os, subprocess, csv
import numpy as np
import pandas as pd
from os.path import isfile, join


class Generator:

    def __init__(self, project_name=None, fault_version=None, work_dir=None, sample_num=None):
        self.d4j_env = os.environ["D4J_HOME"]
        self.pwd = os.getcwd()
        #self.d4j_env = "/mnt/d/workdir/defects4j"
        #self.pwd = "/mnt/c/Data/Chart"

        self.project_name = project_name
        self.fault_version = fault_version
        self.working_directory = work_dir
        self.project_name_lower = self.project_name.lower()
        self.data_category = "/method_data/"
        self.sample_number = sample_num
        self.stmt_path = "/framework/bin/fluccs/method_stmt/"
        self.fault_path = "/framework/bin/fluccs/fault_list/"
        ###
        self.coverage_origin_path = "coverage_data/origin_data/"
        self.coverage_ccm_path = "coverage_data/ccm_data/"
        self.train_data_origin_path = "train_data/origin_data/"
        self.train_data_ccm_path = "train_data/ccm_data/"
        self.train_data_ccm_x_path = "train_data/ccm_x_data/"

        self.graph_data_origin_path = "total_data/graph_data/origin_data/"
        self.graph_data_ccm_path = "total_data/graph_data/ccm_data/"
        self.rank_data_origin_path = "total_data/rank_data/origin_data/"
        self.rank_data_ccm_path = "total_data/rank_data/ccm_data/"
        self.line_path = "method_data/line_data/"

        self.merged_class = None

        self.base_path = self.pwd + "/outputs/" + self.project_name + "/"

        ## generate_coverage_data
        self.pass_coverage_list = []
        self.fail_coverage_list = []
        self.class_group_by = []

    def read_method_info(self):
        method_id_list = []
        method_path = self.d4j_env + self.stmt_path + self.project_name + "/Methods/" + self.project_name + "." + str(
            self.fault_version) + ".csv"
        f = open(method_path, 'r', encoding='utf-8')
        rdr = csv.reader(f)
        for line in rdr:
            method_id_list.append(line[0])
        f.close()

        print("=== Complete === Read Method data")
        return pd.DataFrame(method_id_list)

    def read_method_ccm_info(self):
        read_csv_path = self.pwd + "/" + self.working_directory +"/output/methodAgeAndChurns.csv"
        read_csv = pd.read_csv(read_csv_path, header=None)
        # print(read_csv[0])


        # method_id_list = []
        # method_path = self.d4j_env + self.stmt_path + self.project_name + "/Methods/" + self.project_name + "." + str(
        #     self.fault_version) + ".csv"
        # f = open(method_path, 'r', encoding='utf-8')
        # rdr = csv.reader(f)
        # for line in rdr:
        #     method_id_list.append(line[0])
        # f.close()

        print("=== Complete === Read Method data")
        return pd.DataFrame(read_csv[0])

    def read_statement_info(self):
        stmt_path = self.d4j_env + self.stmt_path + self.project_name + "/stmt_mth_id_" + self.project_name + "_" + str(self.fault_version) + ".csv"
        stmt_mth_id = pd.read_csv(stmt_path, header=None)
        stmt_mth_id = pd.DataFrame(stmt_mth_id).sort_values([0, 1])
        sort_stmt_mth_id = stmt_mth_id.reset_index(drop=True)
        print("=== Complete === Read statement data")
        return sort_stmt_mth_id

    def read_coverage_files(self):

        coverage_path = self.pwd + "/" + self.working_directory + "/test_cases/coverage/"
        file_names = [f for f in os.listdir(coverage_path) if isfile(join(coverage_path, f))]
        coverage_files = [coverage_path + file_name for file_name in file_names]
        print("=== Complete === Read coverage data " + str(self.fault_version) + " fault")
        return coverage_files

    def generate_coverage_data(self, statement_info, coverage_files):
        print("=== Start === Generate coverage data => " + self.project_name + " " + self.fault_version)
        coverage_data_origin_path = self.base_path + self.coverage_origin_path
        method_line_data_path = self.base_path + self.line_path
        if not os.path.exists(coverage_data_origin_path):
            os.makedirs(coverage_data_origin_path)

        if not os.path.exists(method_line_data_path):
            os.makedirs(method_line_data_path)
        pass_save_path = coverage_data_origin_path + "pass_" + self.project_name + "_" + self.fault_version + "_data"
        fail_save_path = coverage_data_origin_path + "fail_" + self.project_name + "_" + self.fault_version + "_data"
        stmt_save_path = method_line_data_path + "stmt_" + self.project_name + "_" + self.fault_version + "_data.csv"

        ########

        process_total_count = len(coverage_files)
        process_count = 0

        for file_name in coverage_files:
            process_count = process_count + 1
            data_frame = pd.read_json(file_name, typ="series")
            refined_data = []

            for key in data_frame.keys():
                if key in ['testcaseName', 'testcaseResult']:
                    value = data_frame[key]
                    refined_data.append((key, value, value))
                    continue
                for k, v in data_frame[key].items():
                    refined_data.append((key, int(k), int(v)))

            coverage_data = pd.DataFrame(refined_data).sort_values([0, 1]).reset_index(drop=True)
            coverage_list = coverage_data[2].tolist()

            coverage_data[0] = statement_info[2]
            class_list_info = coverage_data[:-2]
            class_list = class_list_info[[0, 2]]
            self.class_group_by = class_list.groupby([0], as_index=False).sum()
            self.class_group_by.loc[self.class_group_by[2] > 1, 2] = 1
            self.merged_class = pd.merge(self.read_method_ccm_info(), self.class_group_by, how="left", on=0)
            # print(self.merged_class)
            merged_list = self.merged_class[2].tolist()
            merged_list.append(coverage_list[-1])
            # print(merged_list)

            if coverage_list[-1] == 0:
                self.pass_coverage_list.append(merged_list)
            elif coverage_list[-1] == 1:
                print(file_name)
                self.fail_coverage_list.append(merged_list)

            print(np.asarray(self.pass_coverage_list).shape)
            print(np.asarray(self.fail_coverage_list).shape)
            print(file_name)

            process_percent = (process_count / process_total_count) * 100
            print("processing... ", round(process_percent, 2), "% ===== ", self.project_name_lower,
                  str(self.fault_version), "=====")

        np.save(pass_save_path, np.asarray(self.pass_coverage_list))
        np.save(fail_save_path, np.asarray(self.fail_coverage_list))




        self.merged_class.to_csv(stmt_save_path, header=False)
        print("Create file: ", pass_save_path)
        print("Create file: ", fail_save_path)
        print("Create file: ", stmt_save_path)

        print("=== End === Generate coverage data => " + self.project_name + " " + self.fault_version)

    def generate_coverage_ccm_data(self):
        print("=== Start === Generate coverage code and change data => " + self.project_name)
        # coverage_data_origin_path = self.base_path + self.coverage_origin_path
        # train_data_ccm_path = self.base_path + self.train_data_ccm_path

        folder_path = self.base_path + self.coverage_origin_path
        save_path = self.base_path + self.coverage_ccm_path
        files, files_name = get_filename_in_folder(folder_path)

        if not os.path.exists(save_path):
            os.makedirs(save_path)
        for file, file_name in zip(files, files_name):
            fault_num = file.split("_")[-2]
            # print(fault_num)
            method_line_info = self.read_method_info_num(fault_num)
            method_ccm_info = self.read_method_ccm_num(fault_num)
            merged_data = pd.merge(method_line_info, method_ccm_info, how="left", on=0)
            method_ccm_data = merged_data[[4, 5]]
            old_npy_data = np.load(file)
            if old_npy_data.shape[0] == 0:
                continue
            ccm_npy_data = refine_ccm_data(old_npy_data, method_ccm_data)
            npy_path = file_name.replace("_data.npy", "_data_metrics.npy")
            print("Create file : ", npy_path)
            print(save_path + npy_path)
            np.save(save_path + npy_path, ccm_npy_data)
            print("Create file: ", save_path + npy_path)
        print("=== End === Generate coverage code and change data => " + self.project_name)

    def generate_coverage_ccm_data_min(self):
        print("=== Start === Generate coverage code and change data => " + self.project_name)
        # coverage_data_origin_path = self.base_path + self.coverage_origin_path
        # train_data_ccm_path = self.base_path + self.train_data_ccm_path

        folder_path = self.base_path + self.coverage_origin_path
        save_path = self.base_path + self.coverage_ccm_path
        files, files_name = get_filename_in_folder(folder_path)

        if not os.path.exists(save_path):
            os.makedirs(save_path)
        for file, file_name in zip(files, files_name):
            fault_num = file.split("_")[-2]
            # print(fault_num)
            method_line_info = self.read_method_info_num(fault_num)
            method_ccm_info = self.read_method_ccm_num(fault_num)
            merged_data = pd.merge(method_line_info, method_ccm_info, how="left", on=0)
            method_ccm_data = merged_data[[4, 5]]
            old_npy_data = np.load(file)
            if old_npy_data.shape[0] == 0:
                continue
            ccm_npy_data = refine_ccm_data_min(old_npy_data, method_ccm_data)
            npy_path = file_name.replace("_data.npy", "_data_metrics_min.npy")
            print("Create file : ", npy_path)
            print(save_path + npy_path)
            np.save(save_path + npy_path, ccm_npy_data)
            print("Create file: ", save_path + npy_path)

        print("=== End === Generate coverage code and change data => " + self.project_name)


    def generate_coverage_ccm_data_max(self):
        print("=== Start === Generate coverage code and change data => " + self.project_name)
        # coverage_data_origin_path = self.base_path + self.coverage_origin_path
        # train_data_ccm_path = self.base_path + self.train_data_ccm_path

        folder_path = self.base_path + self.coverage_origin_path
        save_path = self.base_path + self.coverage_ccm_path
        files, files_name = get_filename_in_folder(folder_path)

        if not os.path.exists(save_path):
            os.makedirs(save_path)
        for file, file_name in zip(files, files_name):
            fault_num = file.split("_")[-2]
            # print(fault_num)
            method_line_info = self.read_method_info_num(fault_num)
            method_ccm_info = self.read_method_ccm_num(fault_num)
            merged_data = pd.merge(method_line_info, method_ccm_info, how="left", on=0)
            method_ccm_data = merged_data[[4, 5]]
            old_npy_data = np.load(file)
            if old_npy_data.shape[0] == 0:
                continue
            ccm_npy_data = refine_ccm_data_max(old_npy_data, method_ccm_data)
            npy_path = file_name.replace("_data.npy", "_data_metrics_max.npy")
            print("Create file : ", npy_path)
            print(save_path + npy_path)
            np.save(save_path + npy_path, ccm_npy_data)
            print("Create file: ", save_path + npy_path)

        print("=== End === Generate coverage code and change data => " + self.project_name)

    def generate_train_data(self):
        print("=== Start === Generate train data => " + self.project_name + " " + self.fault_version)

        coverage_data_origin_path = self.base_path + self.coverage_origin_path
        train_data_origin_path = self.base_path + self.train_data_origin_path

        if not os.path.exists(train_data_origin_path):
            os.makedirs(train_data_origin_path)

        pass_xy = np.load(coverage_data_origin_path + "pass_" + self.project_name + "_" + self.fault_version + "_data.npy")
        fail_xy = np.load(coverage_data_origin_path + "fail_" + self.project_name + "_" + self.fault_version + "_data.npy")
        try:
            pass_xy = pass_xy[np.random.choice(pass_xy.shape[0], int(self.sample_number), replace=False)]
        except ValueError:
            print("Check npy data size !!!")
            sys.exit(1)
        if pass_xy.shape[0] > fail_xy.shape[0]:
            weight = int(pass_xy.shape[0] / fail_xy.shape[0])
        else:
            weight = int(fail_xy.shape[0] / pass_xy.shape[0])

        fail_list = fail_xy.tolist()
        pass_list = pass_xy.tolist()
        w_fail_list = fail_list * weight
        merged_xy_list = pass_list + w_fail_list
        merged_xy_np = np.asarray(merged_xy_list)
        save_filename = train_data_origin_path + "train_" + self.project_name + "_" + str(
                self.fault_version) + "_data_" + str(
                self.sample_number)
        np.random.shuffle(merged_xy_np)
        np.save(save_filename, merged_xy_np)

        print("Create file: ", save_filename + ".npy")

        print("=== End === Generate train data => " + self.project_name + " " + self.fault_version)

    def generate_train_ccm_data(self):
        print("=== Start === Generate train data => " + self.project_name + " " + self.fault_version)

        coverage_data_ccm_path = self.base_path + self.coverage_ccm_path
        train_data_ccm_path = self.base_path + self.train_data_ccm_path

        if not os.path.exists(train_data_ccm_path):
            os.makedirs(train_data_ccm_path)
        try:
            pass_xy = np.load(coverage_data_ccm_path + "pass_" + self.project_name + "_" + self.fault_version + "_data_metrics.npy")
            fail_xy = np.load(coverage_data_ccm_path + "fail_" + self.project_name + "_" + self.fault_version + "_data_metrics.npy")
        except FileNotFoundError as e:
            print("File Not Found : ", self.project_name + "_" + self.fault_version, " error :", e)
            sys.exit(1)
        try:
            pass_xy = pass_xy[np.random.choice(pass_xy.shape[0], int(self.sample_number), replace=False)]
        except ValueError:
            print("Check npy data size !!!")
            sys.exit(1)
        if pass_xy.shape[0] > fail_xy.shape[0]:
            weight = int(pass_xy.shape[0] / fail_xy.shape[0])
        else:
            weight = int(fail_xy.shape[0] / pass_xy.shape[0])

        fail_list = fail_xy.tolist()
        pass_list = pass_xy.tolist()
        w_fail_list = fail_list * weight
        merged_xy_list = pass_list + w_fail_list
        merged_xy_np = np.asarray(merged_xy_list)
        np.random.shuffle(merged_xy_np)
        save_filename = train_data_ccm_path + "train_" + self.project_name + "_" + str(self.fault_version) + "_data_" + str(self.sample_number)
        np.save(save_filename, merged_xy_np)
        print("Create file: ", save_filename + ".npy")

        print("=== End === Generate train data => " + self.project_name + " " + self.fault_version)

    def generate_train_ccm_data_min(self):
        print("=== Start === Generate train data => " + self.project_name + " " + self.fault_version)

        coverage_data_ccm_path = self.base_path + self.coverage_ccm_path
        train_data_ccm_path = self.base_path + self.train_data_ccm_path

        if not os.path.exists(train_data_ccm_path):
            os.makedirs(train_data_ccm_path)
        try:
            pass_xy = np.load(coverage_data_ccm_path + "pass_" + self.project_name + "_" + self.fault_version + "_data_metrics_min.npy")
            fail_xy = np.load(coverage_data_ccm_path + "fail_" + self.project_name + "_" + self.fault_version + "_data_metrics_min.npy")
        except FileNotFoundError as e:
            print("File Not Found : ", self.project_name + "_" + self.fault_version, " error :", e)
            sys.exit(1)
        try:
            pass_xy = pass_xy[np.random.choice(pass_xy.shape[0], int(self.sample_number), replace=False)]
        except ValueError:
            print("Check npy data size !!!")
            sys.exit(1)
        if pass_xy.shape[0] > fail_xy.shape[0]:
            weight = int(pass_xy.shape[0] / fail_xy.shape[0])
        else:
            weight = int(fail_xy.shape[0] / pass_xy.shape[0])

        fail_list = fail_xy.tolist()
        pass_list = pass_xy.tolist()
        w_fail_list = fail_list * weight
        merged_xy_list = pass_list + w_fail_list
        merged_xy_np = np.asarray(merged_xy_list)
        np.random.shuffle(merged_xy_np)
        save_filename = train_data_ccm_path + "min_train_" + self.project_name + "_" + str(self.fault_version) + "_data_" + str(self.sample_number)
        np.save(save_filename, merged_xy_np)
        print("Create file: ", save_filename + ".npy")

        print("=== End === Generate train data => " + self.project_name + " " + self.fault_version)

    def generate_train_ccm_data_max(self):
        print("=== Start === Generate train data => " + self.project_name + " " + self.fault_version)

        coverage_data_ccm_path = self.base_path + self.coverage_ccm_path
        train_data_ccm_path = self.base_path + self.train_data_ccm_path

        if not os.path.exists(train_data_ccm_path):
            os.makedirs(train_data_ccm_path)
        try:
            pass_xy = np.load(coverage_data_ccm_path + "pass_" + self.project_name + "_" + self.fault_version + "_data_metrics_max.npy")
            fail_xy = np.load(coverage_data_ccm_path + "fail_" + self.project_name + "_" + self.fault_version + "_data_metrics_max.npy")
        except FileNotFoundError as e:
            print("File Not Found : ", self.project_name + "_" + self.fault_version, " error :", e)
            sys.exit(1)
        try:
            pass_xy = pass_xy[np.random.choice(pass_xy.shape[0], int(self.sample_number), replace=False)]
        except ValueError:
            print("Check npy data size !!!")
            sys.exit(1)
        if pass_xy.shape[0] > fail_xy.shape[0]:
            weight = int(pass_xy.shape[0] / fail_xy.shape[0])
        else:
            weight = int(fail_xy.shape[0] / pass_xy.shape[0])

        fail_list = fail_xy.tolist()
        pass_list = pass_xy.tolist()
        w_fail_list = fail_list * weight
        merged_xy_list = pass_list + w_fail_list
        merged_xy_np = np.asarray(merged_xy_list)
        np.random.shuffle(merged_xy_np)
        save_filename = train_data_ccm_path + "max_train_" + self.project_name + "_" + str(self.fault_version) + "_data_" + str(self.sample_number)
        np.save(save_filename, merged_xy_np)
        print("Create file: ", save_filename + ".npy")

        print("=== End === Generate train data => " + self.project_name + " " + self.fault_version)

    def generate_test_x_data(self):
        print("=== Start === Generate test x data")

        folder_path = self.base_path + self.coverage_ccm_path
        # folder_path = self.pwd + "/output/" + self.project_name + "/method_data/npy_data/metrics_original/"
        files_name = [f for f in os.listdir(folder_path) if isfile(join(folder_path, f))]
        # print(files_name)
        files_name = [f for f in files_name if "fail_" in f]
        files_name = [f for f in files_name if "data_metrics.npy" in f]

        # print(files_name)
        files = [folder_path + file_name for file_name in files_name]
        save_path = self.base_path + self.train_data_ccm_x_path

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        for file, file_name in zip(files, files_name):
            fault_num = file.split("_")[-3]
            method_line_info = self.read_method_info_num(fault_num)
            method_ccm_info = self.read_method_ccm_num(fault_num)
            merged_data = pd.merge(method_line_info, method_ccm_info, how="left", on=0)
            # print(merged_data)
            method_ccm_data = merged_data[[4, 5]]
            churn_data = np.asarray(method_ccm_data[4])
            churn_refine_data = churn_data[~np.isnan(churn_data)]
            # print(churn_refine_data.mean())
            age_data = np.asarray(method_ccm_data[5])
            age_refine_data = age_data[~np.isnan(age_data)]
            # print(age_refine_data.mean())
            old_npy_data = np.load(file)
            x_arr_ = []
            x_data_num = old_npy_data.shape[1] - 3
            x_data = np.eye(x_data_num)

            for num, data in enumerate(x_data):
                np_index = np.where(data == 1)
                churn_value = churn_data[np_index]
                age_value = age_data[np_index]

                churn_min_value = churn_value
                churn_max_value = churn_value
                age_min_value = age_value
                age_max_value = age_value

                if np.isnan(churn_value):
                    # pass
                    # churn_min_value = 0.0
                    # churn_max_value = 1.0
                    churn_min_value = churn_refine_data.mean()
                    churn_max_value = churn_refine_data.mean()
                    # print(churn_min_value)
                if np.isnan(age_value):
                    # pass
                    # age_min_value = 0.0
                    # age_max_value = 1.0
                    age_min_value = age_refine_data.mean()
                    age_max_value = age_refine_data.mean()

                ## 2 column basic
                refine_x_data = np.append(data, (churn_min_value, age_min_value))

                ## 4 column min, max churn
                # refine_x_data = np.append(data, (churn_min_value, churn_max_value, age_min_value, age_max_value))
                x_arr_.append(refine_x_data)

            x_refine_data = np.asarray(x_arr_)
            npy_path = "test_" + self.project_name + "_" + str(fault_num) + "_data.npy"
            print("Create file: ", npy_path)
            np.save(save_path + npy_path, x_refine_data)

        print("=== End === Generate test x data")

    def generate_test_x_data_min(self):
        print("=== Start === Generate test x data")

        folder_path = self.base_path + self.coverage_ccm_path
        # folder_path = self.pwd + "/output/" + self.project_name + "/method_data/npy_data/metrics_original/"
        files_name = [f for f in os.listdir(folder_path) if isfile(join(folder_path, f))]
        # print(files_name)
        files_name = [f for f in files_name if "fail_" in f]

        files_name = [f for f in files_name if "_min" in f]

        # print(files_name)
        files = [folder_path + file_name for file_name in files_name]
        save_path = self.base_path + self.train_data_ccm_x_path

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        for file, file_name in zip(files, files_name):
            fault_num = file.split("_")[-4]
            method_line_info = self.read_method_info_num(fault_num)
            method_ccm_info = self.read_method_ccm_num(fault_num)
            merged_data = pd.merge(method_line_info, method_ccm_info, how="left", on=0)
            # print(merged_data)
            method_ccm_data = merged_data[[4, 5]]
            churn_data = np.asarray(method_ccm_data[4])
            churn_refine_data = churn_data[~np.isnan(churn_data)]
            # print(churn_refine_data.mean())
            age_data = np.asarray(method_ccm_data[5])
            age_refine_data = age_data[~np.isnan(age_data)]
            # print(age_refine_data.mean())
            old_npy_data = np.load(file)
            x_arr_ = []
            x_data_num = old_npy_data.shape[1] - 3
            x_data = np.eye(x_data_num)

            for num, data in enumerate(x_data):
                np_index = np.where(data == 1)
                churn_value = churn_data[np_index]
                age_value = age_data[np_index]

                churn_min_value = churn_value
                # churn_max_value = churn_value
                age_min_value = age_value
                # age_max_value = age_value

                if np.isnan(churn_value):
                    pass
                    # churn_min_value = 0.0
                    # churn_max_value = 1.0
                    # churn_min_value = churn_refine_data.mean()
                    # churn_max_value = churn_refine_data.mean()
                    # print(churn_min_value)
                if np.isnan(age_value):
                    pass
                    # age_min_value = 0.0
                    # age_max_value = 1.0
                    # age_min_value = age_refine_data.mean()
                    age_max_value = age_refine_data.mean()

                ## 2 column basic
                refine_x_data = np.append(data, (churn_min_value, age_min_value))

                ## 4 column min, max churn
                # refine_x_data = np.append(data, (churn_min_value, churn_max_value, age_min_value, age_max_value))
                x_arr_.append(refine_x_data)

            x_refine_data = np.asarray(x_arr_)
            npy_path = "min_test_" + self.project_name + "_" + str(fault_num) + "_data.npy"
            print("Create file: ", npy_path)
            np.save(save_path + npy_path, x_refine_data)

        print("=== End === Generate test x data")

    def generate_test_x_data_max(self):
        print("=== Start === Generate test x data")

        folder_path = self.base_path + self.coverage_ccm_path
        # folder_path = self.pwd + "/output/" + self.project_name + "/method_data/npy_data/metrics_original/"
        files_name = [f for f in os.listdir(folder_path) if isfile(join(folder_path, f))]
        # print(files_name)
        files_name = [f for f in files_name if "fail_" in f]

        files_name = [f for f in files_name if "_min" in f]

        # print(files_name)
        files = [folder_path + file_name for file_name in files_name]
        save_path = self.base_path + self.train_data_ccm_x_path

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        for file, file_name in zip(files, files_name):
            fault_num = file.split("_")[-4]
            method_line_info = self.read_method_info_num(fault_num)
            method_ccm_info = self.read_method_ccm_num(fault_num)
            merged_data = pd.merge(method_line_info, method_ccm_info, how="left", on=0)
            # print(merged_data)
            method_ccm_data = merged_data[[4, 5]]
            churn_data = np.asarray(method_ccm_data[4])
            churn_refine_data = churn_data[~np.isnan(churn_data)]
            # print(churn_refine_data.mean())
            age_data = np.asarray(method_ccm_data[5])
            age_refine_data = age_data[~np.isnan(age_data)]
            # print(age_refine_data.mean())
            old_npy_data = np.load(file)
            x_arr_ = []
            x_data_num = old_npy_data.shape[1] - 3
            x_data = np.eye(x_data_num)

            for num, data in enumerate(x_data):
                np_index = np.where(data == 1)
                churn_value = churn_data[np_index]
                age_value = age_data[np_index]

                churn_min_value = churn_value
                # churn_max_value = churn_value
                age_min_value = age_value
                # age_max_value = age_value

                if np.isnan(churn_value):
                    pass
                    # churn_min_value = 0.0
                    # churn_max_value = 1.0
                    # churn_min_value = churn_refine_data.mean()
                    # churn_max_value = churn_refine_data.mean()
                    # print(churn_min_value)
                if np.isnan(age_value):
                    pass
                    # age_min_value = 0.0
                    # age_max_value = 1.0
                    # age_min_value = age_refine_data.mean()
                    # age_max_value = age_refine_data.mean()

                ## 2 column basic
                refine_x_data = np.append(data, (churn_min_value, age_min_value))

                ## 4 column min, max churn
                # refine_x_data = np.append(data, (churn_min_value, churn_max_value, age_min_value, age_max_value))
                x_arr_.append(refine_x_data)

            x_refine_data = np.asarray(x_arr_)
            npy_path = "max_test_" + self.project_name + "_" + str(fault_num) + "_data.npy"
            print("Create file: ", npy_path)
            np.save(save_path + npy_path, x_refine_data)

        print("=== End === Generate test x data")

    def generate_statement_fault_data(self):
        print("=== Start === Generate fault data")
        fault_data_path = self.base_path + "method_data/fault_data/"
        base_path_d4j = self.d4j_env + "/framework/projects/" + self.project_name

        if not os.path.exists(fault_data_path + "origin_csv/"):
            os.makedirs(fault_data_path + "origin_csv/")
        if not os.path.exists(fault_data_path + "origin_raw/"):
            os.makedirs(fault_data_path + "origin_raw/")

        files = [f for f in os.listdir(base_path_d4j + "/patches") if isfile(join(base_path_d4j + "/patches", f))]
        for file in files:
            if "src" in file:
                bug_id = file.split(".")[0]
                bug_data_dir = self.pwd + "/" + self.project_name_lower + '_' + bug_id + "_b"
                commit_bug_hash_id, commit_fix_hash_id = extract_commit_db_info(bug_id, base_path_d4j)
                extracted_filename = extract_filename(read_files(file, base_path_d4j))

                bug_commit_cmd = "git ls-tree -r --name-only " + commit_bug_hash_id + " | grep /" + extracted_filename
                fix_commit_cmd = "git ls-tree -r --name-only " + commit_fix_hash_id + " | grep /" + extracted_filename

                try:
                    bugged_file_path = subprocess.check_output(bug_commit_cmd, shell=True, cwd=bug_data_dir).decode(
                        "ISO-8859-1").strip()
                    fixed_file_path = subprocess.check_output(fix_commit_cmd, shell=True, cwd=bug_data_dir).decode(
                        "ISO-8859-1").strip()
                    git_diff_cmd = "git diff -u " + commit_bug_hash_id + ":" + bugged_file_path + " " + commit_fix_hash_id + ":" + fixed_file_path
                    git_diff_info = subprocess.check_output(git_diff_cmd, shell=True, cwd=bug_data_dir).decode(
                        "ISO-8859-1").strip()

                    write_file = open(fault_data_path + "origin_raw/stmt_" + file, 'w')
                    write_file.write(git_diff_info)
                    write_file.close()

                except subprocess.CalledProcessError:
                    continue
                # print("bug_id : ", bug_id)
                fault_list = self.extract_line_info([x.strip() for x in (git_diff_info.splitlines())])

                if not fault_list:
                    continue

                fault_df = pd.DataFrame(fault_list)
                # print(fault_df)
                fault_df.to_csv(
                    fault_data_path + "origin_csv/stmt_fault_" + file.replace(".src.patch", ".csv"), header=None,
                    index=False)

    def generate_method_fault_data(self):
        stmt_mth_id_path = self.d4j_env + self.stmt_path + self.project_name
        method_info_path = self.base_path + self.line_path

        file_names = [f for f in os.listdir(stmt_mth_id_path) if isfile(join(stmt_mth_id_path, f))]
        stmt_file_names = [f for f in file_names if "stmt_mth_id_" in f]
        stmt_mth_id_names = [stmt_mth_id_path + "/" + file_name for file_name in stmt_file_names]
        fault_ids = [split_names.split("stmt_mth_id_" + self.project_name + "_")[-1] for split_names in
                     stmt_file_names]
        # print(fault_ids)
        fault_names = [name.split(".csv")[0] for name in fault_ids]
        # print(fault_names)
        # fault_info_path = self.pwd + "/output/" + self.project_name + self.data_category + "patch_data_csv"
        fault_info_path = self.base_path + "method_data/fault_data/" + "origin_csv"
        for (stmt_mth_id_name, fault_number) in zip(stmt_mth_id_names, fault_names):
            method_line_info_file = method_info_path + "stmt_" + self.project_name + "_" + str(fault_number) + "_data.csv"
            # print(fault_number)
            try:
                fault_info = pd.read_csv(fault_info_path + "/stmt_fault_" + fault_number + ".csv", header=None)
                stmt_mth_id = pd.read_csv(stmt_mth_id_name, header=None)
                stmt_mth_id.columns = ["class_name", "line_number", "method_name"]
                # print(stmt_mth_id)
                fault_info.columns = ["class_name", "line_number", "fault_flag"]
                # print(fault_info)
                method_line_info = pd.read_csv(method_line_info_file, header=None)
                method_line_info = method_line_info[[0, 1]]
                method_line_info.columns = ["line_number", "method_name"]
            except (FileNotFoundError, ValueError):
                continue
            result = pd.merge(stmt_mth_id, fault_info,  on=["class_name", "line_number"])
            # print(fault_info)
            # print(result)
            # print(result)

            result = result.drop(columns=["class_name", "line_number"])
            result = result.drop_duplicates(["method_name"])
            # print(result)
            result = result.dropna()
            try :
                result.columns = ["method_name", "fault_info"]
                merged_data = pd.merge(method_line_info, result, how="left", on="method_name")
                merged_data = merged_data.fillna(0)
                # print(merged_data.loc[merged_data["fault_info"] == 1])

            except pd.errors.EmptyDataError as e:
                print(e)
                continue
            # print(merged_data)
            merged_data.to_csv(fault_info_path + "/method_fault_" + self.project_name + "_" + str(fault_number) + "_.csv", header=None, index=False)
            # result.to_csv(fault_info_path + "/method_fault_" + fault_number + ".csv", header=None, index=False)
        print("=== End === Generate fault data")

    def get_method_fault_data(self):
        method_fault_path = self.d4j_env + self.fault_path + self.project_name
        method_info_path = self.base_path + self.line_path
        fault_info_path = self.base_path + "method_data/fault_data/existing_csv"

        if not os.path.exists(fault_info_path):
            os.makedirs(fault_info_path)

        all_file_names = [f for f in os.listdir(method_fault_path) if isfile(join(method_fault_path, f))]
        file_names = [f for f in all_file_names if "buggy_methods_" + self.project_name in f]
        method_fault_files = [method_fault_path + "/" + file_name for file_name in file_names]

        # print(file_names)
        for method_fault_file in method_fault_files:
            fault_number = method_fault_file.split("_")[-2]
            method_line_info_file = method_info_path + "stmt_" + self.project_name + "_" + str(
                fault_number) + "_data.csv"
            try:
                method_line_info = pd.read_csv(method_line_info_file, header=None)
                method_line_info = method_line_info[[0, 1]]
                method_line_info.columns = ["line_number", "method_name"]
            except FileNotFoundError as e:
                print(e)
                continue

            try:
                method_fault_info = pd.read_csv(method_fault_file, header=None)
                # print(method_fault_info)
                method_fault_info['1'] = 1
                method_fault_info.columns = ["method_name", "fault_info"]
                merged_data = pd.merge(method_line_info, method_fault_info, how="left", on="method_name")
                merged_data = merged_data.fillna(0)
                # print(merged_data.loc[merged_data["fault_info"] == 1])

            except pd.errors.EmptyDataError as e:
                print(e, method_fault_file)
                continue
            # print(merged_data)
            merged_data.to_csv(
                fault_info_path + "/method_fault_" + self.project_name + "_" + str(fault_number) + "_.csv", header=None,
                index=False)

        print("=== End === Generate fault data")

    def read_method_info_num(self, fault_num):
        method_id_list = []
        csv_path = self.pwd + "/outputs/" + self.project_name + "/method_data/line_data"
        method_path = csv_path + "/stmt_" + self.project_name + "_" + str(fault_num) + "_data.csv"

        f = open(method_path, 'r', encoding='utf-8')
        rdr = csv.reader(f)
        for line in rdr:
            method_id_list.append(line[1])
        f.close()
        return pd.DataFrame(method_id_list)

    def read_method_ccm_num(self, fault_num):
        output_path = self.pwd + "/" + self.project_name_lower + "_" + str(fault_num) + "_b/output"
        ccm_path = output_path + "/methodAgeAndChurns.csv"
        stmt_mth_id = pd.read_csv(ccm_path, header=None)
        stmt_mth_id = pd.DataFrame(stmt_mth_id)
        return stmt_mth_id

    def extract_line_info(self, contents):

        # print(contents)
        class_name = ""
        line_number = 0
        fault_list = []
        count = 0
        # print(contents)
        if self.project_name == "Closure":
            # print("Closure")
            for line in contents:

                if "--- a" in line:
                    start = line.find("java/") + 5
                    end = line.find(".java", start)
                    sub_class = line[start:end]
                    replace_class_name = sub_class.replace("/", ".")
                    if ":" in replace_class_name:
                        class_name = replace_class_name.split(":")[-1]
                    else:
                        class_name = replace_class_name

                    class_name = class_name[4:]

                if "@@ " in line:
                    count = 0
                    start = line.find("@@ ") + 3
                    end = line.find(" @@", start)
                    sub_line = line[start:end]
                    sub_start = sub_line.find("-") + 1
                    sub_end = sub_line.find(",", sub_start)
                    line_number = int(sub_line[sub_start:sub_end])

                if "+   " in line:
                    continue

                if "-   " in line:
                    line_numbers = line_number + count
                    fault_list.append([class_name, line_numbers - 1, 1])

                count = count + 1

        if self.project_name == "Lang":
            # print("Hello")
            for line in contents:

                if "--- a" in line:
                    start = line.find("java/") + 5
                    end = line.find(".java", start)
                    sub_class = line[start:end]
                    replace_class_name = sub_class.replace("/", ".")
                    if ":" in replace_class_name:
                        class_name = replace_class_name.split(":")[-1]
                    else:
                        class_name = replace_class_name

                if "@@ " in line:
                    count = 0
                    start = line.find("@@ ") + 3
                    end = line.find(" @@", start)
                    sub_line = line[start:end]
                    sub_start = sub_line.find("-") + 1
                    sub_end = sub_line.find(",", sub_start)
                    line_number = int(sub_line[sub_start:sub_end])

                if "+   " in line:
                    continue

                if "-   " in line:
                    line_numbers = line_number + count
                    fault_list.append([class_name, line_numbers - 1, 1])

                count = count + 1

        return fault_list

def refine_ccm_data(old_npy_data, merged_data):
    churn_data = np.asarray(merged_data[4])
    age_data = np.asarray(merged_data[5])
    basic_npy_data_float = old_npy_data.astype(float)
    ccm_array = []

    for num, data in enumerate(basic_npy_data_float):
        coverage_data = data[:-1]
        fault_data = data[-1]
        np_index = np.where(coverage_data == 1)
        np_list_index = list(np_index[0])
        if not np_list_index:
            continue
        churn_values = churn_data[np_index]
        churn_values = churn_values[~np.isnan(churn_values)]
        age_values = age_data[np_index]
        age_values = age_values[~np.isnan(age_values)]

        ## 2 column : mean value churn, age
        npy_ccm_data = np.append(coverage_data, (churn_values.mean(),  age_values.mean(), fault_data))

        ## 4 column : min, max value
        # npy_ccm_data = np.append(coverage_data, (min(churn_values), max(churn_values), min(age_values), max(age_values), fault_data))

        ccm_array.append(npy_ccm_data)
    ccm_npy_data = np.asarray(ccm_array)
    return ccm_npy_data


def refine_ccm_data_min(old_npy_data, merged_data):
    churn_data = np.asarray(merged_data[4])
    age_data = np.asarray(merged_data[5])
    basic_npy_data_float = old_npy_data.astype(float)
    ccm_array = []

    for num, data in enumerate(basic_npy_data_float):
        coverage_data = data[:-1]
        fault_data = data[-1]
        np_index = np.where(coverage_data == 1)
        np_list_index = list(np_index[0])
        if not np_list_index:
            continue
        churn_values = churn_data[np_index]
        churn_values = churn_values[~np.isnan(churn_values)]
        age_values = age_data[np_index]
        age_values = age_values[~np.isnan(age_values)]

        ## 2 column : mean value churn, age
        npy_ccm_data = np.append(coverage_data, (min(churn_values),  min(age_values), fault_data))

        ## 4 column : min, max value
        # npy_ccm_data = np.append(coverage_data, (min(churn_values), max(churn_values), min(age_values), max(age_values), fault_data))

        ccm_array.append(npy_ccm_data)
    ccm_npy_data = np.asarray(ccm_array)
    return ccm_npy_data

def refine_ccm_data_max(old_npy_data, merged_data):
    churn_data = np.asarray(merged_data[4])
    age_data = np.asarray(merged_data[5])
    basic_npy_data_float = old_npy_data.astype(float)
    ccm_array = []

    for num, data in enumerate(basic_npy_data_float):
        coverage_data = data[:-1]
        fault_data = data[-1]
        np_index = np.where(coverage_data == 1)
        np_list_index = list(np_index[0])
        if not np_list_index:
            continue
        churn_values = churn_data[np_index]
        churn_values = churn_values[~np.isnan(churn_values)]
        age_values = age_data[np_index]
        age_values = age_values[~np.isnan(age_values)]

        ## 2 column : mean value churn, age
        npy_ccm_data = np.append(coverage_data, (max(churn_values),  max(age_values), fault_data))

        ## 4 column : min, max value
        # npy_ccm_data = np.append(coverage_data, (min(churn_values), max(churn_values), min(age_values), max(age_values), fault_data))

        ccm_array.append(npy_ccm_data)
    ccm_npy_data = np.asarray(ccm_array)
    return ccm_npy_data


def get_filename_in_folder(folder_path):
    file_names = [f for f in os.listdir(folder_path) if isfile(join(folder_path, f))]
    npy_files = [folder_path + file_name for file_name in file_names]
    return npy_files, file_names


def extract_commit_db_info(bid, base_path):
    cmd = "grep \"^" + str(bid) + ",\" " + base_path + "/commit-db"
    output = subprocess.check_output(cmd, shell=True, )
    output = output.decode("ISO-8859-1")
    rev_id = output.split(',')[1:]
    rev_id = [x.strip() for x in rev_id]
    return rev_id


def read_files(file, base_path):
    with open(base_path + "/patches/" + file, encoding="ISO-8859-1") as f:
        contents = f.readlines()
    contents = [x.strip() for x in contents]
    return contents


def extract_filename(contents):
    count = 0
    for line in contents:
        count = count + 1
        if "diff" in line:
            contents = line.split("/", )
            return contents[-2] + "/" + contents[-1]





if __name__ == '__main__':
    generator = Generator(project_name="Chart", fault_version="1", work_dir="chart_1_b", sample_num="2000")
    # generator.generate_coverage_ccm_data_min()
    # generator.generate_coverage_ccm_data_max()
    # generator.generate_test_x_data_min()
    # generator.generate_test_x_data_max()
    # generator.generate_coverage_data(generator.read_statement_info(), generator.read_coverage_files())
    # generator.read_method_ccm_info()
    # generator.generate_test_x_data()
    # generator.generate_coverage_data(generator.read_statement_info(), generator.read_coverage_files())
    # generator.generate_test_x_data()
    # generator.generate_coverage_ccm_data()
    # generator.generate_train_data()
    # generator.generate_train_ccm_data()
    # generator.generate_statement_fault_data()
    # generator.generate_method_fault_data()
    # generator.get_method_fault_data()
